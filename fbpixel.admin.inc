<?php

/**
 * @file
 * Administrative page callbacks for the fbpixel module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function fbpixel_admin_settings_form($form_state) {
  $form['fbpixel_default'] = array(
    '#type' => 'textfield',
    '#title' => t('Site-wide default Facebook Pixel ID'),
    '#default_value' => variable_get('fbpixel_default', ''),
    '#maxlength' => 20,
  );

  // Page specific visibility configurations.
  $visibility = variable_get('fbpixel_visibility_pages', 0);
  $pages = variable_get('fbpixel_pages', FBPIXEL_PAGES);

  $form['tracking']['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $options = array(
    t('Every page except the listed pages'),
    t('The listed pages only'),
  );
  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));
  $title = t('Pages');
  $form['tracking']['page_vis_settings']['fbpixel_visibility_pages'] = array(
    '#type' => 'radios',
    '#title' => t('Add Facebook Pixel to specific pages'),
    '#options' => $options,
    '#default_value' => $visibility,
  );
  $form['tracking']['page_vis_settings']['fbpixel_pages'] = array(
    '#type' => 'textarea',
    '#title' => $title,
    '#title_display' => 'invisible',
    '#default_value' => $pages,
    '#description' => $description,
    '#rows' => 10,
  );
  return system_settings_form($form);
}
