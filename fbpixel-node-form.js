
(function ($) {

Drupal.behaviors.fbpixelFieldsetSummaries = {
  attach: function (context) {
    $('fieldset.fbpixel-node-settings-form', context).drupalSetSummary(function (context) {
      var vals = [];

      // Option
      var option = $(".form-item-fbpixel .form-select option:selected", context);
      if (option) {
        vals.push(option.text());
      }
      
      // Override.
      if (option.val() == 1) {
        var pixelId = $(".form-item-fbpixel-override .form-text", context).val();
        if (pixelId) {
          vals.push(pixelId);
        }
      }
      
      return Drupal.checkPlain(vals.join(': '));
    });

    // Provide the summary for the node type form.
    $('fieldset.fbpixel-node-type-settings-form', context).drupalSetSummary(function(context) {
      var vals = [];

      // Default pixel ID.
      var pixelId = $(".form-item-fbpixel .form-text", context).val();
      if (pixelId) {
        vals.push(pixelId);
      }
      
      // Override.
      var override = $(".form-item-fbpixel-override input:checked", context).next('label').text();
      if (override) {
        vals.push(override);
      }

      return Drupal.checkPlain(vals.join(', '));
    });
  }
};

})(jQuery);
